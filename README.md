# personal-code

# Context

The code I chose is `useUrlParamsFilter.ts` with `CompaniesTable.tsx` & `UserTable.tsx` being usage examples which were part of cms website that used lots of tables and filters.

# Explanation

The hooks was designed as a plug and play solution on a page containing a table with a filter. Through the hook the filter inputs were serialized to a url and deserialized from a url to a filter, resulting in sharable links with pedetermined filters set.

# Assessment

2 of the 4 props required by the hook were necessary when creating a table with a filter and the deserialized map and serialize map were easily addable, making the hook easily reusable. The typing is not great due to my lack of knowledge of final-forms typing system.
