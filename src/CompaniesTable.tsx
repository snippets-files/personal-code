import { useQuery } from "@apollo/client";
import {
    createPagePagingActions,
    Field,
    FinalFormRadio,
    SortDirection,
    StackSwitchApiContext,
    Table as AdminTable,
    TableFilterFinalForm,
    TableQuery,
    useTableQuery,
    useTableQueryFilter,
    useTableQueryPaging,
    useTableQuerySort,
} from "@comet/admin";
import { FormControlLabel, Typography } from "@material-ui/core";
import { QueryInput } from "app/components/";
import Icon, { Icons } from "app/components/common/Icon";
import AddButton from "app/components/common/pageContent/AddButton";
import PageContent from "app/components/common/pageContent/PageContent";
import { EditButton } from "app/components/common/table";
import Actions from "app/components/common/table/Actions";
import DeleteButton from "app/components/common/table/DeleteButton";
import { useMemoizedTableVariables } from "app/components/common/table/useMemoizedTableVariables";
import { companiesListQuery, deleteCompanyMutation } from "app/components/companies/CompaniesTable.gql";
import * as sc from "app/components/companies/CompaniesTable.sc";
import { CompaniesList, CompaniesListVariables } from "app/components/companies/generated/CompaniesList";
import { UserMeContext } from "app/contexts/UserMe";
import { useUrlParamsFilter } from "app/hooks";
import { mapValues, reduce } from "lodash";
import * as React from "react";
import { FormattedMessage, useIntl } from "react-intl";

import { CompanySubscriptionFilter, UserRole } from "../../../generated/globalTypes";
import TenantField from "../common/TenantField";
import { TenantCompany } from "../users/generated/TenantCompany";
import { tenantCompanyQuery } from "../users/TenantCompany.gql";

interface IFilterVariables {
    query: string;
    tenant: { id: string; value: string };
    subscriptionFilter: CompanySubscriptionFilter;
}

interface IUrlParams {
    tenant: string;
    query: string;
}

export const CompaniesTable: React.FC = () => {
    const stackApi = React.useContext(StackSwitchApiContext);
    const userMe = React.useContext(UserMeContext);
    const pagingApi = useTableQueryPaging(1);
    const sortApi = useTableQuerySort({ columnName: "name", direction: SortDirection.ASC });
    const filterApi = useTableQueryFilter<Partial<IFilterVariables>>({ subscriptionFilter: CompanySubscriptionFilter.ALL }, { pagingApi });
    const { query, subscriptionFilter } = filterApi.current;
    const [selectedTenantId, setSelectedTenantId] = React.useState<string>();
    const memoizedVariables = useMemoizedTableVariables<CompaniesListVariables>({ pagingApi, sortApi });
    const intl = useIntl();

    const { data: tenantCompanyList } = useQuery<TenantCompany>(tenantCompanyQuery, {
        variables: { tenantId: selectedTenantId },
        fetchPolicy: "cache-and-network",
    });

    const { tableData, api, loading, error } = useTableQuery<CompaniesList, CompaniesListVariables>()(companiesListQuery, {
        resolveTableData: ({ companiesList }) => ({
            data: companiesList.nodes,
            totalCount: companiesList.totalCount,
            pagingInfo: createPagePagingActions(pagingApi, companiesList),
        }),
        variables: { ...memoizedVariables, tenantId: selectedTenantId, query, subscriptionFilter },
    });

    const mapFilterApiToParams = React.useCallback(() => {
        return mapValues(filterApi.current, (o) => {
            if (typeof o === "string") return o;
            if (o?.id) return o.id;
        });
    }, [filterApi]);
    const mapParamsToFilterApi = React.useCallback((params: IUrlParams) => {
        return reduce<IUrlParams, Partial<IFilterVariables>>(
            params,
            (result, value, key: keyof IUrlParams) => {
                if (key !== "query") result[key] = { id: value, value: "" };
                else result[key] = value;
                return result;
            },
            {},
        );
    }, []);

    const { initialFilter } = useUrlParamsFilter<Partial<IFilterVariables>, Partial<IUrlParams>>(
        filterApi,
        mapFilterApiToParams,
        mapParamsToFilterApi,
        tenantCompanyList,
    );

    React.useEffect(() => {
        if (initialFilter?.tenant?.id) {
            setSelectedTenantId(initialFilter.tenant.id);
        }
    }, [initialFilter]);
    return (
        <>
            <TableQuery api={api} loading={loading} error={error}>
                <PageContent
                    title={intl.formatMessage({ id: "wlp.comon.companies", defaultMessage: "Unternehmen" })}
                    buttons={
                        userMe.user?.role === UserRole.ADMIN && (
                            <AddButton
                                onClick={() => {
                                    stackApi.activatePage("form", "add");
                                }}
                            >
                                <FormattedMessage id="wlp.components.companies.createCompanyText" defaultMessage="Neues Unternehmen erstellen" />
                            </AddButton>
                        )
                    }
                >
                    <TableFilterFinalForm filterApi={filterApi}>
                        <sc.FilterWrapper>
                            <sc.FieldWrapper>
                                <TenantField
                                    onChange={(value) => {
                                        setSelectedTenantId(value);
                                    }}
                                    isRequired={false}
                                    isClearable
                                />
                            </sc.FieldWrapper>
                            <sc.FieldWrapper>
                                <Field
                                    name="query"
                                    component={QueryInput}
                                    endAdornment={<Icon icon={Icons.Search} fontSize="inherit" />}
                                    fieldContainerComponent={sc.Container}
                                />
                            </sc.FieldWrapper>
                            <sc.RadioGroup>
                                <Field
                                    type="radio"
                                    name="subscriptionFilter"
                                    value={CompanySubscriptionFilter.ALL}
                                    fieldContainerComponent={sc.RadioFieldContainer}
                                >
                                    {(props) => (
                                        <FormControlLabel
                                            labelPlacement="end"
                                            label={intl.formatMessage({
                                                id: "wlp.common.all",
                                                defaultMessage: "Alle",
                                            })}
                                            control={<FinalFormRadio {...props} />}
                                        />
                                    )}
                                </Field>
                                <Field
                                    type="radio"
                                    name="subscriptionFilter"
                                    value={CompanySubscriptionFilter.FREE}
                                    fieldContainerComponent={sc.RadioFieldContainer}
                                >
                                    {(props) => (
                                        <FormControlLabel
                                            labelPlacement="end"
                                            label={intl.formatMessage({
                                                id: "wlp.common.freeCompany",
                                                defaultMessage: "Testmodus",
                                            })}
                                            control={<FinalFormRadio {...props} />}
                                        />
                                    )}
                                </Field>
                                <Field
                                    type="radio"
                                    name="subscriptionFilter"
                                    value={CompanySubscriptionFilter.CUSTOMER}
                                    fieldContainerComponent={sc.RadioFieldContainer}
                                >
                                    {(props) => (
                                        <FormControlLabel
                                            labelPlacement="end"
                                            label={intl.formatMessage({
                                                id: "wlp.common.customer",
                                                defaultMessage: "Kunde",
                                            })}
                                            control={<FinalFormRadio {...props} />}
                                        />
                                    )}
                                </Field>
                            </sc.RadioGroup>
                        </sc.FilterWrapper>
                    </TableFilterFinalForm>
                    {tableData && (
                        <AdminTable
                            {...tableData}
                            sortApi={sortApi}
                            columns={[
                                {
                                    name: "name",
                                    header: intl.formatMessage({ id: "wlp.comon.name", defaultMessage: "Name" }),
                                    cellProps: { size: "medium" },
                                    sortable: true,
                                    render: ({ name }) => <Typography>{name}</Typography>,
                                },
                                {
                                    name: "tenant",
                                    header: intl.formatMessage({ id: "wlp.company.tentant.name", defaultMessage: "Mandant" }),
                                    cellProps: { size: "medium" },
                                    sortable: true,
                                    render: ({ tenant }) => <Typography>{tenant.name}</Typography>,
                                },
                                {
                                    name: "hasValidSubscription",
                                    header: intl.formatMessage({ id: "wlp.company.freeCompany", defaultMessage: "Testmodus" }),
                                    cellProps: { size: "medium" },
                                    sortable: false,
                                    render: ({ hasValidSubscription }) => (
                                        <Typography>
                                            {hasValidSubscription === false
                                                ? intl.formatMessage({ id: "wlp.common.label.yes", defaultMessage: "ja" })
                                                : intl.formatMessage({ id: "wlp.common.label.no", defaultMessage: "nein" })}
                                        </Typography>
                                    ),
                                },
                                {
                                    name: "edit",
                                    cellProps: { size: "medium", align: "right" },
                                    render: (row) => (
                                        <Actions
                                            lastAction={
                                                userMe.user?.role === UserRole.ADMIN && (
                                                    <DeleteButton
                                                        mutation={deleteCompanyMutation}
                                                        confirmationMessage={intl.formatMessage(
                                                            {
                                                                id: "wlp.company.delete.confirmationMessage",
                                                                defaultMessage:
                                                                    "Du bist dabei, das Unternehmen `{companyName}` inkl. {userAmount} User zu löschen.",
                                                            },
                                                            { companyName: row.name, userAmount: row.totalUsers },
                                                        )}
                                                        confirmationToTypeText={row.shortName}
                                                        selectedId={String(row.id)}
                                                    />
                                                )
                                            }
                                        >
                                            <EditButton
                                                onClick={() => {
                                                    stackApi.activatePage("form", String(row.id));
                                                }}
                                            />
                                        </Actions>
                                    ),
                                },
                            ]}
                        />
                    )}
                </PageContent>
            </TableQuery>
        </>
    );
};
