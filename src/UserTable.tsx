import { useQuery } from "@apollo/client";
import {
    createPagePagingActions,
    FinalFormRadio,
    ITableColumn,
    SortDirection,
    StackSwitchApiContext,
    Table as AdminTable,
    TableFilterFinalForm,
    TableQuery,
    useTableQuery,
    useTableQueryFilter,
    useTableQueryPaging,
    useTableQuerySort,
} from "@comet/admin";
import { Field } from "@comet/admin";
import { Button, FormControlLabel, Typography, useTheme } from "@material-ui/core";
import { QueryInput } from "app/components/";
import CompanyField from "app/components/common/CompanyField";
import Icon, { Icons } from "app/components/common/Icon";
import IconStatus from "app/components/common/IconStatus";
import PageContent from "app/components/common/pageContent/PageContent";
import { EditButton } from "app/components/common/table";
import { useMemoizedTableVariables } from "app/components/common/table/useMemoizedTableVariables";
import TenantField from "app/components/common/TenantField";
import * as sc from "app/components/users/UserTable.sc";
import { getApiUrl } from "app/config";
import { getActivationTypeDescription, getUserRoleDescription, UserMeContext } from "app/contexts/UserMe";
import { useFreeCompany, useUrlParamsFilter } from "app/hooks";
import { client } from "app/utils/http";
import { getFreeUserString } from "app/utils/intl";
import * as FileSaver from "file-saver";
import { mapValues, reduce } from "lodash";
import * as React from "react";
import { FormattedDate, FormattedMessage, FormattedTime, useIntl } from "react-intl";

import { UserRole, UserVerificationFilter } from "../../../generated/globalTypes";
import { IconButton } from "../MasterHeader.sc";
import { AddUserButton } from "./AddUserButton";
import { TenantCompany } from "./generated/TenantCompany";
import { Users, Users_usersList_nodes, UsersVariables } from "./generated/Users";
import { tenantCompanyQuery } from "./TenantCompany.gql";
import { usersQuery } from "./Users.gql";
import { useUsersExportAsCsv } from "./users.hooks";

interface IFilterVariables {
    company: { id: string; value: string };
    query: string;
    tenant: { id: string; value: string };
    verificationFilter: UserVerificationFilter;
}

interface IUrlParams {
    tenant: string;
    company: string;
    query: string;
}

export const UserTable: React.FC = () => {
    const intl = useIntl();
    const stackApi = React.useContext(StackSwitchApiContext);
    const userMeContext = React.useContext(UserMeContext);
    const [selectedTenantId, setSelectedTenantId] = React.useState<string>();

    const pagingApi = useTableQueryPaging(1);
    const sortApi = useTableQuerySort({ columnName: "name", direction: SortDirection.ASC });
    const filterApi = useTableQueryFilter<Partial<IFilterVariables>>({ verificationFilter: UserVerificationFilter.ALL }, { pagingApi });

    const { data: tenantCompanyList } = useQuery<TenantCompany>(tenantCompanyQuery, {
        variables: { tenantId: selectedTenantId },
        fetchPolicy: "cache-only",
    });

    const mapFilterApiToParams = React.useCallback(() => {
        return mapValues(filterApi.current, (o) => {
            if (typeof o === "string") return o;
            if (o?.id) return o.id;
        });
    }, [filterApi]);
    const mapParamsToFilterApi = React.useCallback((params: IUrlParams) => {
        return reduce<IUrlParams, Partial<IFilterVariables>>(
            params,
            (result, value, key: keyof IUrlParams) => {
                if (key !== "query") result[key] = { id: value, value: "" };
                else result[key] = value;
                return result;
            },
            {},
        );
    }, []);

    const { initialFilter } = useUrlParamsFilter<Partial<IFilterVariables>, Partial<IUrlParams>, TenantCompany>(
        filterApi,
        mapFilterApiToParams,
        mapParamsToFilterApi,
        tenantCompanyList,
    );

    const theme = useTheme();
    const { isFreeCompany, remainingFreeUsers } = useFreeCompany();

    React.useEffect(() => {
        if (initialFilter?.tenant?.id) {
            setSelectedTenantId(initialFilter.tenant.id);
        }
    }, [initialFilter]);

    const memoizedVariables = useMemoizedTableVariables<UsersVariables>({ pagingApi, sortApi });

    const { tenant, company, query, verificationFilter } = filterApi.current;

    const { tableData, api, loading, error } = useTableQuery<Users, UsersVariables>()(usersQuery, {
        resolveTableData: ({ usersList }) => ({
            data: usersList.nodes,
            totalCount: usersList.totalCount,
            pagingInfo: createPagePagingActions(pagingApi, usersList),
        }),
        variables: {
            ...memoizedVariables,
            companyIds: company?.id === "allCompanies" || !company ? [] : [company.id],
            tenantId: tenant?.id,
            query,
            verificationFilter,
        },
        fetchPolicy: "network-only",
    });

    const columns: ITableColumn<Users_usersList_nodes>[] = [];
    columns.push({
        name: "name",
        header: intl.formatMessage({ id: "chp.common.name", defaultMessage: "Name" }),
        cellProps: { size: "medium" },
        sortable: true,
        render: ({ firstname, lastname }: Users_usersList_nodes) => (
            <Typography>
                {firstname} {lastname}
            </Typography>
        ),
    });
    columns.push({
        name: "email",
        header: intl.formatMessage({ id: "chp.common.email", defaultMessage: "E-Mail" }),
        sortable: false,
        render: ({ email }: Users_usersList_nodes) => <Typography>{email}</Typography>,
    });

    if (userMeContext.user?.role === UserRole.ADMIN) {
        columns.push({
            name: "company",
            header: intl.formatMessage({ id: "chp.users.companyMandat", defaultMessage: "Unternehmen / Mandant" }),
            cellProps: { size: "medium" },
            sortable: false,
            render: ({ company }: Users_usersList_nodes) => (
                <>
                    <Typography variant="body2">{company.name}</Typography>
                    <Typography variant="body2">{company.tenant.name}</Typography>
                </>
            ),
        });
    }
    columns.push({
        name: "role",
        header: intl.formatMessage({ id: "chp.users.role", defaultMessage: "Rolle" }),
        sortable: true,
        render: ({ role }: Users_usersList_nodes) => <Typography variant="body2">{getUserRoleDescription(role)}</Typography>,
    });

    columns.push({
        name: "activationType",
        header: intl.formatMessage({ id: "chp.users.activationType", defaultMessage: "Aktivierungstyp" }),
        sortable: true,
        render: ({ activationType }: Users_usersList_nodes) => (
            <Typography variant="body2">{getActivationTypeDescription(activationType)}</Typography>
        ),
    });
    columns.push({
        name: "emailVerifiedAt",
        header: intl.formatMessage({ id: "chp.users.verifiedAt", defaultMessage: "Verifiziert am" }),
        sortable: true,
        render: ({ emailVerifiedAt }: Users_usersList_nodes) =>
            emailVerifiedAt && (
                <Typography variant="body2">
                    <FormattedDate value={emailVerifiedAt} day="2-digit" month="2-digit" year="numeric" /> <FormattedTime value={emailVerifiedAt} />
                </Typography>
            ),
    });
    columns.push({
        name: "isVerified",
        header: intl.formatMessage({ id: "wlp.common.status", defaultMessage: "Status" }),
        cellProps: { size: "medium" },
        sortable: false,
        render: ({ isVerified }) => (
            <>
                {isVerified ? (
                    <IconStatus icon={<Icon icon={Icons.PublishedYes} htmlColor={theme.palette.success.main} />}>
                        {intl.formatMessage({ id: "wlp.common.verified", defaultMessage: "Verifiziert" })}
                    </IconStatus>
                ) : (
                    <IconStatus icon={<Icon icon={Icons.PublishedNo} htmlColor={theme.palette.error.main} />}>
                        {intl.formatMessage({ id: "wlp.common.notVerified", defaultMessage: "Nicht verifiziert" })}
                    </IconStatus>
                )}
            </>
        ),
    });
    if (userMeContext.user?.role === UserRole.ADMIN) {
        columns.push({
            name: "data",
            header: intl.formatMessage({ id: "wlp.components.users.dataExport", defaultMessage: "Daten-Export" }),
            cellProps: { size: "medium" },
            render: ({ id }) => (
                <IconButton
                    onClick={async () => {
                        client
                            .get(`${getApiUrl()}/users/${id}/export`, { responseType: "blob" })
                            .then((response) => FileSaver.saveAs(response.data, "export.zip"));
                    }}
                >
                    <Icon icon={Icons.ImportExport} />
                </IconButton>
            ),
        });
    }
    columns.push({
        name: "edit",
        cellProps: { size: "medium", align: "right" },
        render: ({ id }) => (
            <EditButton
                onClick={() => {
                    stackApi.activatePage("edit", String(id));
                }}
            />
        ),
    });

    const { exportUsersAsCsv, isExportLoading } = useUsersExportAsCsv(verificationFilter, company?.id, query, tenant?.id);

    const userRole = userMeContext.user?.role;
    const pageContentButton =
        userRole && [UserRole.RESTRICTED_ADMIN, UserRole.ADMIN, UserRole.COMPANY_MANAGER].includes(userRole) ? (
            <AddUserButton remainingFreeUsers={remainingFreeUsers} />
        ) : undefined;

    return (
        <PageContent title={intl.formatMessage({ id: "chp.users.userAccounts", defaultMessage: "Benutzer-Accounts" })} buttons={pageContentButton}>
            {isFreeCompany && (
                <sc.FreeAccountInviteInfoBox>
                    {intl.formatMessage(
                        {
                            id: "wlp.freeCompany.userInfo",
                            defaultMessage: `${getFreeUserString()} Bereits eingeladene Nutzer kannst du unter "bearbeiten" löschen.`,
                        },
                        { remainingFreeUsers },
                    )}
                </sc.FreeAccountInviteInfoBox>
            )}
            <TableQuery api={api} loading={loading} error={error}>
                <TableFilterFinalForm filterApi={filterApi}>
                    <sc.FilterWrapper>
                        {userMeContext.user?.role !== UserRole.COMPANY_MANAGER && (
                            <>
                                <sc.FieldWrapper>
                                    <TenantField
                                        onChange={(value) => {
                                            setSelectedTenantId(value);
                                        }}
                                        isRequired={false}
                                    />
                                </sc.FieldWrapper>
                                <sc.FieldWrapper>
                                    <CompanyField selectedTenantId={selectedTenantId} isRequired={false} />
                                </sc.FieldWrapper>
                            </>
                        )}
                        <sc.FieldWrapper>
                            <Field
                                name="query"
                                component={QueryInput}
                                endAdornment={<Icon icon={Icons.Search} fontSize="inherit" />}
                                fieldContainerComponent={sc.Container}
                            />
                        </sc.FieldWrapper>
                        <sc.RadioGroup>
                            <Field
                                type="radio"
                                name="verificationFilter"
                                value={UserVerificationFilter.ALL}
                                fieldContainerComponent={sc.RadioFieldContainer}
                            >
                                {(props) => (
                                    <FormControlLabel
                                        labelPlacement="end"
                                        label={intl.formatMessage({
                                            id: "wlp.common.all",
                                            defaultMessage: "Alle",
                                        })}
                                        control={<FinalFormRadio {...props} />}
                                    />
                                )}
                            </Field>
                            <Field
                                type="radio"
                                name="verificationFilter"
                                value={UserVerificationFilter.VERIFIED}
                                fieldContainerComponent={sc.RadioFieldContainer}
                            >
                                {(props) => (
                                    <FormControlLabel
                                        labelPlacement="end"
                                        label={intl.formatMessage({
                                            id: "wlp.common.verifiedUser",
                                            defaultMessage: "Verifizierte",
                                        })}
                                        control={<FinalFormRadio {...props} />}
                                    />
                                )}
                            </Field>
                            <Field
                                type="radio"
                                name="verificationFilter"
                                value={UserVerificationFilter.UNVERIFIED}
                                fieldContainerComponent={sc.RadioFieldContainer}
                            >
                                {(props) => (
                                    <FormControlLabel
                                        labelPlacement="end"
                                        label={intl.formatMessage({
                                            id: "wlp.common.unverifiedUser",
                                            defaultMessage: "Nicht verifizierte",
                                        })}
                                        control={<FinalFormRadio {...props} />}
                                    />
                                )}
                            </Field>
                        </sc.RadioGroup>
                        <sc.ExportButtonWrapper>
                            <Button
                                color="primary"
                                variant="contained"
                                disabled={isExportLoading}
                                startIcon={<Icon icon={Icons.ImportExport} />}
                                onClick={exportUsersAsCsv}
                            >
                                <FormattedMessage id="chp.common.exportAsCsv" defaultMessage="Export als CSV" />
                            </Button>
                        </sc.ExportButtonWrapper>
                    </sc.FilterWrapper>
                </TableFilterFinalForm>
                {tableData && <AdminTable {...tableData} sortApi={sortApi} columns={columns} />}
            </TableQuery>
        </PageContent>
    );
};
