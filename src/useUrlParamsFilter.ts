import { IFilterApi } from "@comet/admin";
import { AnyObject } from "final-form";
import { forEach, isEmpty } from "lodash";
import qs from "qs";
import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import { usePrevious } from "./usePrevious";

/**
 * Hook to manage filter in url params
 * writes current filter to url params
 * reads url params and sets current filter
 * filterApi.current    - (e.g. { company: { id: string; value: string}, tenant: { id: string; value: string }, query: string } )
 * url param string     - (e.g. ?tenant=[tenant.id]&company=[company.id]&query=[query])
 * @param filterApi the filter object
 * @param mapFilterApiToParams function that maps filter object to url params
 * @param mapParamsToFilterApi function that maps url params to filter api
 * @param valueMap map of filter keys with ids to values
 */
export function useUrlParamsFilter<FilterValues extends AnyObject, UrlParams extends AnyObject, ValueMap extends AnyObject = {}>(
    filterApi: IFilterApi<FilterValues>,
    mapFilterApiToParams: () => UrlParams,
    mapParamsToFilterApi: (params: UrlParams) => FilterValues,
    valueMap?: ValueMap,
) {
    const location = useLocation();
    const history = useHistory();
    const [initialFilter, setInitialFilter] = React.useState<FilterValues | null>(null);
    const prevInitialFilter = usePrevious(initialFilter);
    const prevFilter = usePrevious(filterApi.current);
    const prevValueMap = usePrevious(valueMap);

    /**
     * set value of Filter field if null
     */
    React.useEffect(() => {
        if (valueMap && valueMap !== prevValueMap) {
            const { batch, change } = filterApi.formApi;

            batch(() => {
                forEach(filterApi.current, (value, key) => {
                    if (typeof value !== "string" && !value.value) {
                        change(key, { ...value, value: valueMap[key].find((o: { id: string; name: string }) => o.id === value.id).name });
                    }
                });
            });
        }
    }, [valueMap, prevValueMap, filterApi.formApi, filterApi]);

    /**
     * get initial filter from url
     */
    React.useEffect(() => {
        if (!initialFilter && isEmpty(filterApi.current)) {
            const search = location.search;
            if (search) {
                const parsedUrlParams = qs.parse(search.substring(1)) as UrlParams;

                setInitialFilter(mapParamsToFilterApi(parsedUrlParams));
            }
        }
    }, [filterApi, initialFilter, location.search, mapParamsToFilterApi]);

    /**
     * applies filter from url query params
     * - only after the initialFilter was first set
     */
    React.useEffect(() => {
        if (prevInitialFilter === null && initialFilter !== prevInitialFilter) {
            const { batch, change } = filterApi.formApi;

            batch(() => {
                forEach(initialFilter, (value, key) => {
                    change(key, value);
                });
            });
        }
    }, [initialFilter, filterApi, filterApi.formApi, prevInitialFilter]);

    /**
     * reads from filter and writes to url
     */
    React.useEffect(() => {
        if (prevFilter !== filterApi.current) {
            const queryParams = mapFilterApiToParams();
            const search = `?${qs.stringify(queryParams)}`;

            if (search !== location.search) {
                history.push({ pathname: location.pathname, search });
            }
        }
    }, [filterApi, history, location.pathname, location.search, mapFilterApiToParams, prevFilter]);

    return { initialFilter };
}
